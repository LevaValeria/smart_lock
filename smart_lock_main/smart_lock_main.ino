#include <SoftwareSerial.h>
#include <OneWire.h>
#include <Servo.h>

#define BUZZ 3
#define LED_RED 4
#define LED_GREEN 5
#define LED_BLUE 6

SoftwareSerial BT(11, 12);
Servo myservo;
OneWire iButton(10);

byte addr[8];
String address = "164152180900208";
String input_address = "";
boolean stringComplete = false;
String inputString = "";
String password = "123456\n";

void openDoor() {
  BT.println("OPEN");
  myservo.write(90);
  ledLight(1, 0, 1);
  delay(100);
}

void closeDoor() {
  BT.println("CLOSE");
  myservo.write(0);
  ledLight(0, 1, 1);
  delay(100);
}
void ledLight(int red, int green, int blue) {
  digitalWrite(LED_RED, red);
  digitalWrite(LED_GREEN, green);
  digitalWrite(LED_BLUE, blue);
}
void sendWrong() {
  BT.println("WRONG");
  alarm();
  ledLight(1, 1, 0);
  delay(100);
}
void alarm() {
  tone(BUZZ, 100);
  delay(200);
  tone(BUZZ, 500);
  delay(200);
  noTone(BUZZ);
}
void setup() {
  Serial.begin(9600);
  BT.begin(9600);
  myservo.attach(9);
  closeDoor();
  pinMode(BUZZ, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);

}

void loop() {
  while (BT.available()) {
    char inChar = BT.read();
    inputString += inChar;
    if (inChar == '\n' || inChar == '\r') {
      stringComplete = true;
    }
  }
  if (stringComplete) {
    delay(100);
    if (inputString == "CLOSE\n") {
      closeDoor();
    }
    else if (inputString == "KEY\n") {
      if (iButton.search(addr)) {
        for (int i = 0; i < 8; i++) {
          input_address += addr[i];
        }
        if (input_address == address) {
          openDoor();
        }
        else {
          sendWrong();
        }
      }
      else {
        BT.println("IDN");
      }
      iButton.reset();
    }
    else if (inputString == password) {
      openDoor();
    }
    else {
      sendWrong();
    }
    inputString = "";
    input_address = "";
    stringComplete = false;
  }

}



