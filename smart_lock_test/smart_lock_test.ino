#include <SoftwareSerial.h>
#include <OneWire.h>
#include <Servo.h>

#define BUZZ 3
#define LED_RED 4
#define LED_GREEN 5
#define LED_BLUE 6

SoftwareSerial BT(11, 12);
Servo myservo;
OneWire iButton(10);

byte addr[8];
String address;
void setup() {
  Serial.begin(9600);
  BT.begin(9600);
  myservo.attach(9);
  pinMode(BUZZ, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  ledLight(1, 1, 1);
}

void loop() {

  while (BT.available() > 0) {
    char c = BT.read();
    Serial.println(c);
    if (c == 'R') {
      ledLight(0, 1, 1);
    }
    if (c == 'G') {
      ledLight(1, 0, 1);
    }
    if (c == 'B') {
      ledLight(1, 1, 0);
    }
    if (c == 'O') {
      myservo.write(0);
    }
    if (c == 'C') {
      myservo.write(90);
    }
    if (c == 'P') {
      alarm();
    }
    if (c == 'I') {
      delay(1000);
      byte addr[8];
      if ( !iButton.search(addr) ) {
        Serial.println("No key connected...");
        BT.println("No key connected...");
        return;
      }

      Serial.print("Key : ");
      for (int i = 0; i < 8; i++) {
        Serial.print(addr[i], HEX);
        Serial.print(" ");
        BT.print(addr[i], HEX);
        BT.print(" ");
      }
      Serial.println();
      iButton.reset();

    }

  }
}

void ledLight(int red, int green, int blue) {
  digitalWrite(LED_RED, red);
  digitalWrite(LED_GREEN, green);
  digitalWrite(LED_BLUE, blue);
}

void alarm() {
  tone(BUZZ, 100);
  delay(200);
  tone(BUZZ, 500);
  delay(200);
  noTone(BUZZ);
}

